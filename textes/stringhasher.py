class StringHasher:
    def __init__(self, hash_factor, compression_factor, compression_summand, buckets):
        self.hash_factor = hash_factor
        self.compression_factor = compression_factor
        self.compression_summand = compression_summand
        self.buckets = buckets
    
    def hash(self, string):
        raw_hash = polynomial_accumulation_hash(self.hash_factor, string, ord)
        return compress(self.compression_factor, self.compression_summand, self.buckets, raw_hash)

def polynomial_accumulation_hash(factor, lst, transform):
    total = 0
    length = len(lst)
    for i in range(length):
        total *= factor
        char_value = transform(lst[length - i - 1])
        total += char_value
    return total

def compress(a, b, N, x):
    return (a * x + b) % N