from stringhasher import *
import random

class HashTable:
    HASH_FACTOR = 41
    size = 0

    def __init__(self, size):
        size = next_prime(size * 2)
        compression_factor = size - 1
        compression_summand = compression_factor - 1
        self.hasher = StringHasher(self.HASH_FACTOR, compression_factor, compression_summand, size)
        self.buckets = []
        for i in range(size):
            self.buckets.append([])

    def get_size(self):
        return self.size

    def get_capacity(self):
        return len(self.buckets)

    def __getitem__(self, key):
        hash = self.hasher.hash(key)
        
        kv_pairs = self.buckets[hash]
        for kvp in kv_pairs:
            if kvp[0] == key:
                return kvp[1]
        return None

    def __setitem__(self, key, value):
        hash = self.hasher.hash(key)
        
        kv_pairs = self.buckets[hash]
        for i in range(len(kv_pairs)):
            kvp = kv_pairs[i]
            if kvp[0] == key:
                # Item already exists, so we replace the value
                kv_pairs[i] = (key, value)
                return
        # Item doesn't exist, so insert it in the bucket and increment size
        kv_pairs.append((key, value))
        self.size += 1

    def __delitem__(self, key):
        hash = self.hasher.hash(key)
        
        kv_pairs = self.buckets[hash]
        for i in range(len(kv_pairs)):
            kvp = kv_pairs[i]
            if kvp[0] == key:
                del kv_pairs[i]
                self.size -= 1
    
    def __contains__(self, item):
        return not self[item] is None

    def __iter__(self):
        for bucket in self.buckets:
            for kvp in bucket:
                yield kvp
    
    def resize(self, new_buckets):
        new_table = HashTable(new_buckets)
        for bucket in self.buckets:
            for kvp in bucket:
                new_table[kvp[0]] = kvp[1]
        self = new_table

    def __str__(self):
        string = "["
        for bucket in self.buckets:
            for kvp in bucket:
                key = kvp[0]
                value = kvp[1]
                string += "(" + str(key) + ", " + str(value) + "), "
        string += "]"
        return string

def is_prime(n):
    """
    Checks division by factors up to sqrt(n) for primality.
    """
    factor = 2
    while factor * factor <= n:
        mod = n % factor
        if (mod == 0):
            return False
        factor += 1
    return True

def next_prime(n):
    """"
    Returns the least prime number equal to or greater than n.
    """
    while not is_prime(n):
        n += 1
    return n