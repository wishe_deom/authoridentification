from hashtable import HashTable

import math
import os

# Sets have average O(1) lookup while lists have O(n)
PUNCTUATION = set(["!",'"',"'",")","(",",",".",";",":","?", "-", "_"])

def strip_punctuation(line):
    """ Deletes all punctuation """
    stripped = ""
    for c in line:
        if c not in PUNCTUATION:
            stripped += c
    return stripped

def minify(words):
    """ Strips short words and makes all lowercase """
    minified = []
    for word in words:
        if len(word) > 2:
            minified.append(word.lower())
    return minified

def strip_and_split(line):
    """
    Separates words and removes punctuation.
    Also ignores short words (length two or less).
    """
    line = strip_punctuation(line)
    line = line.split()
    words = minify(line)
    return words

def get_words_from_file(path):
    """
    Get all words from a file, processed by strip_and_split
    """
    words = []
    with open(path, encoding="utf8") as f:
        lines = f.readlines()
        for line in lines:
            line = line.rstrip()
            if line == '':
                continue
            words += strip_and_split(line)
    return words

def get_text_paths():
    """
    Returns a list of all text file paths in a directory.
    """
    paths = []
    my_path = os.path.realpath(__file__)
    my_dir, _ = os.path.split(my_path)
    for file in os.listdir(my_dir):
        if not file.endswith(".txt"):
            continue
        path = os.path.join(my_dir, file)
        paths.append(path)
    return paths

def get_pair_counts(words):
    """
    Returns a hash table mapping word pairs to frequency.
    """
    table = HashTable(len(words))
    for i in range(len(words) - 1):
        pair = words[i] + " " + words[i + 1]
        if (table[pair] is None):
            table[pair] = 0
        table[pair] += 1
    return table

def compute_similarity_score(t0, t1):
    """
    Returns the root mean square deviation between word pair counts of two tables
    """
    common = HashTable(t0.get_size() + t1.get_size())
    total0 = 0
    total1 = 0
    for kvp in t0:
        key = kvp[0]
        count0 = kvp[1]
        count1 = t1[key]
        if not count1 is None:
            common[key] = (count0, count1)
            total0 += count0
            total1 += count1

    total = 0
    for bucket in common.buckets:
        for kvp in bucket:
            counts = kvp[1]
            difference = counts[0] / total0 - counts[1] / total1
            total += difference ** 2
    mean_square = total / common.get_size()
    return math.sqrt(mean_square)

def extract_author_name(path):
    ext = ".txt"
    dir, filename = os.path.split(path)
    if filename.endswith(ext):
        return filename[:-len(ext)].title()