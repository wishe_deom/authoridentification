from hashtable import HashTable
from util import *

import os
import sys

text_paths = get_text_paths()

my_dir, _ = os.path.split(os.path.realpath(__file__))
MYSTERY_PATH = os.path.join(my_dir, "mystere.txt")

count_table = HashTable(len(text_paths))

for path in text_paths:
    text = get_words_from_file(path)
    count_table[path] = get_pair_counts(text)

min_score = None
min_author = None

for path in text_paths:
    if path == MYSTERY_PATH:
        continue
    score = compute_similarity_score(count_table[path], count_table[MYSTERY_PATH])
    author = extract_author_name(path)
    print(author + ": " + str(score))
    if (min_score is None or score < min_score):
        min_score = score
        min_author = author

print("Mystery author: " + min_author)